## Setup do projeto
* Instale o NodeJS versão 6.11.2;
* Instale os packages com o comando 'npm install' na pasta raiz do projeto (local);
* Para executar os testes da aplicação rode o comando 'npm test';
* Para executar a aplicação rode o comando 'npm start';

## Considerações
* A api está preparada para aceitar json no body das requisições;
* O serviço foi desenvolvido utilizando NodeJs na versão 6.11.2;
* Estamos fazendo uma utilização simplista de banco de dados em memória;

## Documentação da api
### Criação da conta - post /accounts
Este endpoint é chamado pelo backend quando um novo cadastro de empresa é criado. 

#### Exemplo de requisição em curl
```text
  curl -X POST \
  http://localhost:5877/accounts \
  -H 'content-type: application/json' \
  -d '{
    "code": "95868",
    "name": "Teste"
}'
```

### Débito - post /accounts/:code/debit
Este endpoint é chamado pelo microsserviço de COURSES quando um novo usuário adquire um novo curso utilizando a cota da empresa.

#### Exemplo de requisição em curl

```text
curl -X POST \
  http://localhost:5877/accounts/12345/debit \
  -H 'content-type: application/json' \
  -d '{
    "value": 199
}'
```

### Crédito - post /accounts/:code/credit
Este endpoint é chamado pelo API do gateway externo de pagamento quando a empresa faz um crédito através da interface para empresas.

#### Exemplo de requisição em curl

```text
curl -X POST \
  http://localhost:5877/accounts/12345/credit \
  -H 'content-type: application/json' \
  -d '{
    "value": 199
}'
```

### Saldo - post /accounts/:code/balance
Este endpoint é chamado pela interface para empresas toda vez que o cliente corporativo queira verificar o saldo disponível em sua conta.

#### Exemplo de requisição em curl

```text
curl -X GET \
  http://localhost:5877/accounts/12345/balance \
  -H 'content-type: application/json' \
  -d '{
    "accountCode": "5532"
}'
```