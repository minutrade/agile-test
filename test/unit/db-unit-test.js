const assert = require('chai').assert;
const db = require('../../lib/db');

describe('Teste unitário - Classe de acesso a banco', () => {
  describe('Casos de sucesso', () => {
    it('Deve retornar a conta quando ela for criada', (done) => {
      const code = '99999';
      const name = 'Marcus Costa';

      db.createAccount(code, name, (err, account) => {
        assert.isNull(err);
        assert.equal(code, account.code);
        assert.equal(name, account.name);
        assert.equal(0, account.balance);
        done();
      });
    });
  });
  describe('Casos de erro', () => {
    it('Deve retornar erro quando a conta já existir e tentar criar a mesma', (done) => {
      const code = '99999';
      const name = 'Marcus Costa';

      db.createAccount(code, name, () => {
        db.createAccount(code, name, (err, account) => {
          assert.isUndefined(account);
          assert.equal('Account already exists', err);
          done();
        });
      });
    });
  });
});
