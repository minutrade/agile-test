const app = require('./../../index');
const assert = require('chai').assert;
const supertest = require('supertest');

describe('Teste funcional - Rota POST /accounts/:code/credit', () => {
  describe('Casos de sucesso', () => {
    it('Deve retornar status 200', (done) => {
      supertest(app)
        .post('/accounts/12345/credit')
        .send({ value: 50 })
        .end((err, res) => {
          assert.isNull(err);
          assert.equal(200, res.statusCode);
          done();
        });
    });
  });
});
