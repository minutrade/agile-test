const db = require('./../db');

module.exports = (req, res) => {
  const accountCode = req.params.code;
  const value = req.body.value || 0;

  console.log(`POST /accounts/${accountCode}/credit - ${JSON.stringify(req.body)}`);

  db.creditAccount(accountCode, value, (err, acc) => {
    if (err) {
      console.log(err);
      return res.status(404).send(err);
    }
    console.log(`Balance: ${acc.balance}`);
    return res.status(200).send(`Balance: ${acc.balance}`);
  });
};
