const db = require('./../db');

module.exports = (req, res) => {
  const accountCode = req.params.code;
  const account = req.account;
  const value = req.body.value;

  console.log(`POST /accounts/${accountCode}/debit - ${JSON.stringify(req.body)}`);

  if (account.balance > value) {
    db.debitAccount(accountCode, value, (err, acc) => {
      if (err) {
        console.log(err);
        return res.status(404).send(err);
      }
      console.log(`Balance: ${acc.balance}`);
      return res.status(200).send(`Balance: ${acc.balance}`);
    });
  } else {
    console.log('Insufficient balance');
    return res.status(400).send('Insufficient balance');
  }
};
