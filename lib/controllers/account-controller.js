const db = require('./../db');

const validate = (req, res, next) => {
  const accountCode = req.params.code;
  db.findAccount(accountCode, (err, account) => {
    if (err) {
      console.log(`Error trying to get account by code ${accountCode}`);
      return res.status(404).send(`Error trying to get account by code ${accountCode}`);
    }
    if (!account) {
      console.log('Invalid account');
      return res.status(404).send('Invalid account');
    }
    console.log('Valid account found!');
    req.account = account;
    next();
  });
};

const create = (req, res) => {
  const accountCode = req.body.code;
  const accountName = req.body.name;

  console.log(`POST /accounts - ${JSON.stringify(req.body)}`);

  db.createAccount(accountCode, accountName, (err, account) => {
    if (err) {
      console.log(err);
      return res.status(409).send(err);
    }
    console.log(`Created account!! ${JSON.stringify(account)}`);
    return res.status(201).send(account);
  });
};

module.exports = {
  validate,
  create,
};
