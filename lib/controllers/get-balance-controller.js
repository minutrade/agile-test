const db = require('./../db');

module.exports = (req, res) => {
  const accountCode = req.params.code;

  console.log(`GET /accounts/${accountCode}/balance`);

  if (accountCode) {
    db.findAccount(accountCode, (err, account) => {
      if (account) {
        console.log(`Actual balance is ${account.balance}`);
        res.status(200).send(`Actual balance is ${account.name}`);
      } else {
        console.log('Invalid account');
        res.status(404).send('Invalid account');
      }
    });
  }
};
